#!/usr/bin/env python

from pyspark.sql import SparkSession
from datetime import datetime
spark = SparkSession\
.builder\
.appName('wap')\
.master('yarn')\
.enableHiveSupport()\
.getOrCreate()

now = datetime.now()
yday  = long(now.strftime('%s')) - 3*24*60*60
yday_date = datetime.fromtimestamp(yday).strftime('%Y%m%d')

now_minus_3hrs  = long(now.strftime('%s')) - 5*60*60
now_date = datetime.fromtimestamp(now_minus_3hrs).strftime('%Y%m%d')
now_hour = datetime.fromtimestamp(now_minus_3hrs).strftime('%H')
print("WAP")
#The below prints your results to your chosen destination (Hive, Stdout, CSV)
cmd = '''
insert overwrite table keenek1.radius_conc_sessions
select emsisdn, sessionid, start_time, endtime as end_time,  (unix_timestamp(endtime) - unix_timestamp(start_time))/60 as session_in_secs from(
select emsisdn, sessionid, timestamp as start_time, cmdcode, lead(timestamp,1) over(partition by emsisdn, sessionid order by timestamp) as endtime
from udsapp.radius
where dt > ''' + yday_date + ''' and (cmdcode like '%CCAT%' or cmdcode like '%CCAI%'))
where cmdcode like '%CCAI%'
'''
spark.sql(cmd)