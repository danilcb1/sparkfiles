import smtplib
from datetime import datetime
from subprocess import check_output
import dask.delayed as delay
import time

rtime = datetime.now()
now  = datetime.now() 
nowz  = long(rtime.strftime('%s'))
yday  = long(rtime.strftime('%s')) - 2*24*60*60
date = datetime.fromtimestamp(nowz).strftime('%Y-%m-%d')
yday_date = datetime.fromtimestamp(yday).strftime('%Y-%m-%d')

start_time = time.time()

import pandas as pd
df = pd.read_csv('/home/keenek1/monitoring/output.csv', names=["Date", "Table", "Rows"])

#convert to html
email = " <h3>Source Tables:</h3> {df}"
email = email.format(df=df.to_html())

#log, hive, pylocation, project name, should be updated for today or yesterday (remember, when data is updated for yesterday, it's still written today)
files = (
('/home/danilcb1/logs/summary_cuk.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/ra_daily_summary_cuk_dom | head -2', '/home/danilcb1/py_files/ra', 'RA KPI REPORT', 'today', 'not partitioned', 10, 'Daily'),
('/home/keenek1/analytics/logs/new_unlimited.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/unlimited_summary_business | head -2', '/home/keenek1/analytics/py_files/new_unlimited.py', 'Unlimited - Business', 'today', 'partitioned', 10, 'Daily'),
('/home/keenek1/eckoh.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/eckoh_pagesy | head -2', '/home/keenek1/analytics/py_files/eckoh2.py', 'Eckoh Pages', 'today', 'partitioned', 10, 'Daily'),
('/home/danilcb1/logs/summary_dise.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/ra_daily_summary_dise_dom | head -2', '/home/danilcb1/py_files/ra', 'RA KPI REPORT', 'today', 'not partitioned', 10, 'Daily'),
('/home/keenek1/analytics/logs/dpi.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/curated_dpi_data | head -2', '/home/keenek1/analytics/py_files/certified_dpi_data.py', 'DPI Report', 'today', 'partitioned', 06, 'Daily'),
('/home/keenek1/analytics/logs/cf_day.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/cf_day | head -2', '/home/keenek1/analytics/py_files/cf_day_view.py', 'Single Day CF', 'today', 'not partitioned', 06, 'Daily'),
('/home/keenek1/analytics/logs/proptima.log', 'hdfs dfs -ls -t /user/keenek1/proptima_tables | head -2', '/shared/proptima/dab/bin/proptime_pull.sh', 'Performance Reports', 'today', 'not partitioned', 04, 'Hourly'),
('/home/keenek1/analytics/logs/new_unlimited.log', ' hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/unlimited_summary_consumerx | head -2', '/home/keenek1/analytics/py_files/new_unlimited.py', 'New Unlimited Reporting', 'today', 'partitioned', 07, 'Daily'),
('/home/keenek1/analytics/logs/iphne.log', 'hdfs dfs -ls -t /user/hive/warehouse/tableau_analytics.db/iphone_sw_adption | head -2', '/home/keenek1/analytics/py_files/iphone_sw.py', 'iPhone SW Version', 'today', 'partitioned', 07, 'Daily'),
('/home/keenek1/analytics/logs/weblogmaster.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/weblog_master_atm_c_filter_partitioned3 | head -2', '/home/keenek1/analytics/py_files/weblogs_master3.py', 'ATM CF', 'today', 'partitioned', 01, 'Hourly'),
('/home/keenek1/analytics/logs/ipfrdoms.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/domain_monitoring_ipfr | head -2', '/home/keenek1/analytics/py_files/enhanced_doms_ipfr.py', 'IPFR Domain Monitor', 'today', 'partitioned', 07, 'Daily'),
('/home/keenek1/analytics/logs/ipfrdoms.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/daily_top_doms_ipfr | head -2', '/home/keenek1/analytics/py_files/enhanced_doms_ipfr.py', 'IPFR Domain Monitor', 'today', 'not partitioned', 07, 'Daily'),
('/home/keenek1/analytics/logs/weblogmaster.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/vpx_report | head -2', '/home/keenek1/analytics/py_files/weblogs_master3.py', 'IPFR CF', 'today', 'partitioned', 01, 'Hourly'),
#('/home/keenek1/analytics/logs/unlimited.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/unlimitedusers2 | head -2', '/home/keenek1/analytics/py_files/usage_by_bill_cycle2.py', 'Unlimited Reporting', 'today', 'not partitioned', 01, 'Daily'),
#('/home/keenek1/analytics/logs/unlimited.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/unlimited_summary2 | head -2', '/home/keenek1/analytics/py_files/usage_by_bill_cycle2.py', 'Unlimited Reporting', 'today', 'partitioned', 07, 'Daily'),
('/home/keenek1/analytics/logs/radius.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/radius_heartbeat | head -2', '/home/keenek1/analytics/py_files/radius_heartbeat.py', 'Radius Diagnostics', 'today', 'partitioned', 06, 'Daily'),
('/home/keenek1/analytics/logs/wap.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/wap_monitoringx | head -2', '/home/keenek1/analytics/py_files/wap1.py', 'WAP Usage', 'today', 'partitioned', 05, 'Daily'),
('/home/keenek1/analytics/logs/ra_call_sms.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/sms_call_monitor2 | head -2', '/home/keenek1/analytics/py_files/ra_call_sms.py', 'SMS Call Monitor', 'today', 'partitioned', 06, 'Daily'),
('/home/keenek1/analytics/logs/enforcementx.log', 'hdfs dfs -ls -t /user/hive/warehouse/sandvine.db/pcrf_enforcement_live | head -2', '/home/keenek1/analytics/py_files/enforcementx.py', 'Threshold Enforcement Reporting', 'today', 'partitioned', 07, 'Daily'),
('/home/keenek1/analytics/logs/bb.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/blackberry_dailyx | head -2', '/home/keenek1/analytics/py_files/daily_blackberry.py ', 'BB Roaming', 'today', 'partitioned', 07, 'Daily'),
('/home/keenek1/analytics/logs/bb.log', 'hdfs dfs -ls -t /user/hive/warehouse/tableau_analytics.db/bb_totalsy | head -2', '/home/keenek1/analytics/py_files/daily_blackberry.py ', 'BB Roaming', 'today', 'partitioned', 07, 'Daily'),
('/home/keenek1/analytics/logs/ios12.log', 'hdfs dfs -ls -t /user/hive/warehouse/tableau_analytics.db/ios12_releasey| head -2', '/home/keenek1/analytics/py_files/ios12x.py ', 'iOS Push Notifications', 'today', 'partitioned', '09', 'Daily'),
('/home/keenek1/analytics/logs/regional.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/regional_interests| head -2', '/home/keenek1/analytics/py_files/regional_interests.py ', 'Regional Interests', 'today', 'partitioned', 07, 'Daily'),
('/home/keenek1/analytics/logs/sgsn.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/partner_sgsn| head -2', '/home/keenek1/analytics/py_files/partner_sgsn.py ', 'Partner SGSN', 'today', 'partitioned', '09', 'Daily'),
('/home/keenek1/analytics/logs/use.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/bill_cycle_analysis | head -2', '/home/keenek1/analytics/py_files/usage_by_bill_cycle.py ', 'Usage By Tariff', 'today', 'partitioned', 15, 'Daily'),
('/home/keenek1/analytics/logs/mktroam.log', 'hdfs dfs -ls -t /user/hive/warehouse/keenek1.db/roaming_marketing | head -2', '/home/keenek1/analytics/py_files/roaming_marketing_view.py ', 'Roaming vs Domestic Usage', 'today', 'partitioned', 10, 'Daily')
)

results = []
successes = []

@delay
def read_logs(filename):
    with open(filename, 'r') as filex:
        data = filex.read().replace('\n', '')    
        return data 

@delay
def check_last_modified(hdfs, update, partitioned, frequency, hour):
    out = check_output(hdfs, shell=True)
    out = "|".join(out.split())
    name = out.split('|')[10]
    modified_time =  out.split('|')[9]
    modified_time = modified_time[0:2]
    
    #Returns DIR_COUNT, FILE_COUNT, CONTENT_SIZE, PATHNAME
    if partitioned == 'partitioned':
        command = 'hdfs dfs -count ' + name 
        size = check_output(command, shell=True)    
        size =  "|".join(size.split())
        size = size.split('|')[2]

    	if update == 'today':
            if date in out and int(size) >0:
                status='OK'
                successes.append(task_name)
            else:
          	status='Fail'
        elif update == 'yday' and int(size) >0:
            if yday_date in out:
               status='OK'
            else:
               status = 'Fail'
        if status == 'Fail':
            print(name + ' check partition: ' + status)
    
    else:
        if update == 'today':
            if date in out:
                status='OK'
                successes.append(task_name)
            else:
                status='Fail'
        elif update == 'yday':
            if yday_date in out:
               status='OK'
            else:
               status = 'Fail'
        if status == 'Fail':
                print(name + ' check file: ' + status)

    if status == 'Fail' and hour >= timecheck:
        print('time check ' + str(timecheck))
        print('hour ' + str(hour))
        status = 'OK'
 
    if frequency == 'Hourly' and status == 'OK':
        timecheck == now.hour-2
        if timecheck < modified_time:
            status == 'OK'
        else:
            status == 'Fail'
        print(name + ' check hourly: ' + status)
    return status

for file in files:
    filename = file[0]
    hdfs = file[1]
    location = file[2]
    task_name = file[3]
    update = file[4]
    partitioned = file[5]
    hour = int(file[6])
    timecheck = now.hour
    frequency = file[7]
    data = delay(read_logs)(filename).compute()
    
    status = delay(check_last_modified)(hdfs, update, partitioned, frequency, hour).compute()

    results.append([file, status, data, location, task_name])

print(results)
results2 = ''
i=0
while i <len(results):
  if results[i][1] == 'Fail':
	results2 = results2 + '<br>' + '<br>' + '<br><b>' + results[i][4] +'</b><br>' + '---------------' + '<br>' + '<b>log content:</b>' '<br>' +  str(results[i][2]) + '<br><br>' + '<b>PY File:</b>' + '<br>' + str(results[i][3])
  i=i+1

if len(results2) == 0:
  results2 = '<h3>No errors here! Well done to all</h3>'

failures = results2.split().count('Fail')
print(failures)

source_issues = "<h3>missing data:</h3>" 

source_tables = ('IPFR', 'dpi_data', 'dpi_datasum', 'web', 'userscatalog', 'web_vsl', 'mpe', 'singleton_user', 'billshock', 'radius', 'net_act')

for table in source_tables:
	if table not in df.values:
		source_issues = source_issues + table + '<br>'

message = """From: From Netpulse Script Monitoring <kieran.keene@o2.com>
To: To Kieran Keene <kieran.keene@o2.com>
MIME-Version: 1.0
Content-type: text/html
Subject: Hourly Netpulse Script Monitoring Results

This is an e-mail message reporting on the status of 24 Netpulse reports <br>
If the logs do not provide useful information, it may mean the scripts have run but the source data is missing.<br>
<h3><font color="red">Unsuccessful Reports</font></h3>
**********************************"""  + str(results2) + '<br>' + '<br><br>' + '<h3><font color="green">SUCCESSFUL REPORTS:</font></h3>' + str(successes) + '<br><br>' +  email + '<br><br>'  + source_issues

#message = "Logs output" + data

print(message)

sender = 'kieran.keene@o2.com'
#receivers = ['kieran.keene@o2.com', 'Bartek.Danilczyk@telefonica.com', 'AnandaKumar.Murugan@telefonica.com', 'michael.cornall@telefonica.com', 'Dominic.Hulewicz@telefonica.com']
receivers = ['kieran.keene@o2.com', 'Bartek.Danilczyk@telefonica.com', 'AnandaKumar.Murugan@telefonica.com', 'michael.cornall@telefonica.com', 'NetpulseSupport@o2.com' ]

#receivers = ['kieran.keene@o2.com']

if len(results2) > -1:
        try:
           smtpObj = smtplib.SMTP('mailhost.uk.pri.o2.com')
           smtpObj.sendmail(sender, receivers, message)
           print("Successfully sent email")
        except SMTPException:
           print("Error: unable to send email")

end_time = time.time()

print(end_time-start_time)