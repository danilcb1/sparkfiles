select *,
rank() over (partition by usernm, session order by start_secs asc) as rank
from(select *,  
case 
when new_culm is null then 'session1'
when new_culm <=4 then 'session1'
when new_culm >4 and new_culm <= 8 then 'session2'
when new_culm >8 and new_culm <= 12 then 'session3'
when new_culm >12 and new_culm <= 16 then 'session4'
when new_culm >16 and new_culm <= 20 then 'session5'
when new_culm >20 then 'session6'
end as session
from
(select *, lag(cumulative_hours,1) over(partition by usernm order by start_secs) as new_culm
from
(select *, SUM(duration/60/60)
over
 (
partition by usernm
   order by start_secs
   rows between unbounded preceding and current row
 ) cumulative_hours
 from (select *, end_secs-start_secs as duration 
from 
(select *,
cast(split(trim(event_time), ':')[0] as int)*60*60 + 
cast(split(trim(event_time), ':')[1] as int)*60 + 
cast(split(trim(event_time), ':')[2] as int) as start_secs,
cast(split(trim(endtime), ':')[0] as int)*60*60 +
cast(split(trim(endtime), ':')[1] as int)*60 +
cast(split(trim(endtime), ':')[2] as int) as end_secs
from (
select *, lead(event_time,1) over(partition by usernm order by event_time) as endtime
from keenek1.fandstest2)x)p)z)l)w)h