#LOAD IN THE DATA, WITH FILEPATH AS A COLUMN
mpe_data = my_spark.read\
    .option("header","false")\
    .option("delimiter", "\t")\
    .csv('hdfs://udsnameservice/data/pcrf/mpe/dt=20191013/*/*/*', final_structure).withColumn("Datacenter", input_file_name())
mpe_data.printSchema()
 
#DEFINE YOUR STRING SPLIT FUNCTION
def stringsplit(strx):
    done = (strx.split('/')[8]).split('=')[1]
    return done
 
#DEFINE YOUR UDF
cleaning = udf(stringsplit, StringType())
 
#ADD A COLUMN WITH THE SPLIT STRING
mpe_data2 = mpe_data.withColumn('DAB', cleaning(mpe_data['datacenter']))
 
#CREATE TEMPORARY VIEW FOR QUERY
mpe_data2.createOrReplaceTempView("mpe")
 