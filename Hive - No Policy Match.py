#!/usr/bin/env python

from pyspark.sql import SparkSession
from datetime import datetime
spark = SparkSession\
.builder\
.appName('wap')\
.master('yarn')\
.enableHiveSupport()\
.getOrCreate()

now = datetime.now()
yday  = long(now.strftime('%s')) - 24*60*60
yday_date = datetime.fromtimestamp(yday).strftime('%Y%m%d')

now_minus_3hrs  = long(now.strftime('%s')) - 5*60*60
now_date = datetime.fromtimestamp(now_minus_3hrs).strftime('%Y%m%d')
now_hour = datetime.fromtimestamp(now_minus_3hrs).strftime('%H')
print("WAP")
#The below prints your results to your chosen destination (Hive, Stdout, CSV)
cmd = '''
insert overwrite table keenek1.no_policy_match
SELECT
dt,
msisdn,
       events_in_day,
       TYPE,
       QUOTA,
       plat,
       dpg
FROM   (SELECT *, CASE WHEN events_in_day[SIZE(events_in_day)-1] =
       'No Policy Match'
       THEN 'FLAG' WHEN events_in_day[SIZE(events_in_day)-1] = '3' AND
       events_in_day[
       SIZE(events_in_day)-2] = 'No Policy Match' THEN 'FLAG' ELSE 'OK' END AS
       flag,
       billing_platform AS plat, quotaname AS QUOTA
        FROM   (SELECT dt, msisdn,
                       Collect_set(event)     AS events_in_day,
                       TYPE,
                       quotaname,
                       entitlement,
                       billing_platform,
                       domestic_product_group AS dpg
                FROM   (SELECT DISTINCT dt, mpe.msisdn,
                                        event,
                                        domestic_product_group,
                                        quotaname,
                                        billing_platform,
                                        CASE
                                          WHEN poolid = '' THEN 'Singleton'
                                          ELSE 'Shared'
                                        END AS TYPE,
                                        entitlement
                        FROM   udsapp.mpe
                               left join (SELECT msisdn,
                                                 'shared' AS entitlement
                                          FROM   udsapp.subscriber_of_a_pool
                                          WHERE  subscriber_of_a_pool.dt = Cast(
                                                 Regexp_replace(Date_sub(
                                                 current_date,
                                                                1),
                                                 '-', ''
                                                 ) AS INT)
                                          UNION ALL
                                          SELECT msisdn,
                                                 'single' AS entitlement
                                          FROM   udsapp.singleton_user
                                          WHERE  singleton_user.dt = Cast(
                                                 Regexp_replace(Date_sub(
                                                 current_date,
                                                                1), '-',
                                                 '') AS
                                                 INT))q
                                      ON mpe.msisdn = q.msisdn
                        WHERE  mpe.dt = Cast(Regexp_replace(Date_sub(
                                             current_date, 1),
                                             '-', '')
                                             AS INT)
                       )t
                GROUP  BY dt, msisdn,
                          quotaname,
                          domestic_product_group,
                          billing_platform,
                          entitlement,
                          TYPE)s
        WHERE  Array_contains(events_in_day, 'No Policy Match'))v
WHERE  flag = 'FLAG' and quotaname <>  '' and (plat <> 'BB_Prepaid' and plat <> 'giffgaff')
'''