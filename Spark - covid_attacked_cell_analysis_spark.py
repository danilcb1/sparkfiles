#!/usr/bin/env python
from pyspark.sql import SparkSession
import pyspark.sql.functions as sqlfunc
import argparse, sys
from pyspark.sql import *
from pyspark.sql.functions import *
from datetime import datetime

yday_date = 20200405
week_ago = 20200329

#create a context that supports hive
def create_session(appname):
    spark_session = SparkSession\
        .builder\
        .appName(appname)\
        .master('yarn')\
        .config("hive.metastore.uris", "thrift://uds-far-mn1.dab.02.net:9083")\
        .enableHiveSupport()\
        .getOrCreate()
    return spark_session

### START MAIN ###
if __name__ == '__main__':
    spark_session = create_session('covid-towers')
    df1 = spark_session.table('udsapp.ipfr')

    #TOTAL USAGE YESTERDAY
    yd_agg = df1.coalesce(1000)\
        .filter((df1.dt == '20200405' )).select('emsisdn', 'csr', 'bytesdl')

    #TOTAL USAGE SAME DAY LAST WEEK
    lw_ttl_agg = df1.coalesce(1000)\
        .filter((df1.dt == '20200329' )).select('emsisdn', 'csr', 'bytesdl')

    #SELECT THE USERS USING THE AFFECTED CELLS SAME DAY LAST WEEK
    lw_agg = df1.coalesce(1000)\
        .filter((df1.dt == '20200329' ) & (col('csr').isin(['519527','510710', '504955', '508044', '508579', '512167', '514439', '511514']))).select('emsisdn', 'csr', 'bytesdl')

    #AGGREGATE TABLES TO SUM BYTES USED
    yday_summary = yd_agg.groupBy('emsisdn').agg(sqlfunc.sum('bytesdl').alias('yday_bytesdl')).withColumnRenamed('emsisdn', 'yday_emsisdn')
    lw_summary = lw_agg.groupBy('emsisdn').agg(sqlfunc.sum('bytesdl').alias('lw_bytesdl')).withColumnRenamed('emsisdn', 'lw_emsisdn')
    lw_ttl_summary = lw_ttl_agg.groupBy('emsisdn').agg(sqlfunc.sum('bytesdl').alias('lw_ttl_bytesdl')).withColumnRenamed('emsisdn', 'lw_ttl_emsisdn')

    #JOIN THE TABLES
    #LEFT JOIN USERS ON CELLS WITH YDAY USAGE
    conditions = [yday_summary.yday_emsisdn == lw_summary.lw_emsisdn]
    conditions2 = [lw_ttl_summary.lw_ttl_emsisdn == lw_summary.lw_emsisdn]
    output = lw_summary.join(yday_summary, conditions, how='left').join(lw_ttl_summary, conditions2, how='left')

    output.createOrReplaceTempView("tt")
    finaldf = spark_session.sql("create table keenek1.covid_cells9999 as select * from tt")