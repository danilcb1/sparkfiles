#!/usr/bin/env python
from pyspark.sql import SparkSession
import pyspark.sql.functions as sqlfunc
import argparse, sys
from pyspark.sql import *
from pyspark.sql.functions import *
from datetime import datetime
from pyspark.sql.functions import lit
from pyspark.sql.types import *

now = datetime.now()
yday  = long(now.strftime('%s')) - 1*24*60*60
yday_date = datetime.fromtimestamp(yday).strftime('%Y%m%d')

radiusday  = long(now.strftime('%s')) - 3*24*60*60
radius_date = datetime.fromtimestamp(radiusday).strftime('%Y%m%d')

#nohup spark-submit --conf spark.driver.maxResultSize=0 --conf spark.network.timeout=800 --driver-memory 40g --executor-memory 70g consolidated_tuned.py >stdout.log 2>stderr.log &

#create a contexit that supports hive
def create_session(appname):
    spark_session = SparkSession\
        .builder\
        .appName(appname)\
        .master('yarn')\
        .enableHiveSupport()\
        .getOrCreate()
    return spark_session

### START MAIN ###
if __name__ == '__main__':
    print('start')
    print(datetime.now())
    '''
    TABLE DEFINITION AND CACHING
    '''
    spark_session = create_session('Combined_Web_IPFR_Radius')
    ipfr_logs = spark_session.sql("Select dt, hour, loc, catid, catreputation, serverport, catgrpid, urlsetmatched, responderaction, urlsetprivate, sid, emsisdn, bytesdl, bytesul, transactiontime, vservername from udsapp.ipfr where dt = " + yday_date ).persist()
    x = ipfr_logs.count()
    print(x)
    web_logs = spark_session.sql("Select dt, vslsessid, customrepgrp, hour, nsvsn, urlfcid, serverport, urlfcgid, urlfcrep, matchedurlset, urlfstprv, urlfrspa, loc, emsisdn, sessionid, optimisedsize, concat(domain,url) as fulldom, domain, nsvsn, version from udsapp.web where dt = " + yday_date ).persist()
    y = web_logs.count()
    print(y)
    radius = spark_session.sql("Select custavp1, emsisdn as emsisdn2, sessionid as sessionid2 from udsapp.radius where cmdcode = 'PCRF_CCAI' and dt > " + radius_date ).persist()
    z = radius.count()
    print(z)
    print('caching complete')
    print(datetime.now())

    #IPFR CF

    ipfr = ipfr_logs.select('loc', 'catreputation', 'dt', 'hour', 'catid', 'catreputation', 'catgrpid', 'serverport', 'urlsetmatched', 'responderaction', 'urlsetprivate', 'sid', 'emsisdn', 'bytesdl', 'bytesul', 'vservername')
    conditions = [ipfr.sid == radius.sessionid2]
    df3 = ipfr.join(radius, conditions, how='left')\
    .groupBy('hour', 'catid', 'catreputation', 'catgrpid', 'vservername', 'loc', 'serverport', 'urlsetmatched', 'responderaction', 'urlsetprivate', 'custavp1')\
    .agg(sqlfunc.sum('bytesdl').alias('bytesdl'), sqlfunc.countDistinct('emsisdn').alias('users'), sqlfunc.count('*').alias('transactions')).coalesce(1000)\
    .createOrReplaceTempView('ipfr')
    ipfr_table_output = spark_session.sql('create table keenek1.ipfr_filtering2_test as  select "ipfr" as type, * from ipfr ')
    print(datetime.now())
    print('ipfr CF complete')
    #WEB CF
    web = web_logs.filter((web_logs.vslsessid == '0')).select('dt', 'customrepgrp', 'hour', 'nsvsn', 'urlfcid', 'serverport', 'urlfcgid', 'urlfcrep', 'matchedurlset', 'urlfstprv', 'urlfrspa', 'loc', 'emsisdn', 'sessionid', 'optimisedsize')
    conditionsx = [web.sessionid == radius.sessionid2]
    web_joined = web.join(radius, conditionsx, how='left').withColumn('actual_shield', when(radius.custavp1.isNull(), web.customrepgrp).otherwise(radius.custavp1))\
    .groupBy('hour', 'urlfcid', 'urlfcrep', 'urlfcgid', 'loc',  'serverport', 'matchedurlset', 'urlfstprv', 'nsvsn', 'actual_shield')\
    .agg(sqlfunc.sum('optimisedsize').alias('bytesdl'), sqlfunc.countDistinct('emsisdn').alias('users'), sqlfunc.count('*').alias('transactions')).coalesce(1000)\
    .createOrReplaceTempView('web')
    web_table_output = spark_session.sql('create table keenek1.web_cf4_test as select "web" as type, * from web')
    print(datetime.now())
    print('web CF complete')
    #ECKOH PAGES AGE VERIFICATION
    eckoh = web_logs.filter((web_logs.fulldom.like('%assets.o2.co.uk/18plusaccess%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/tesco/iwf%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/tesco/pc%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/tesco/blacklist%') | web_logs.fulldom.like('%giffgaff.com/mobile/over18%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/giffgaff/blacklist%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/giffgaff/iwf%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/iwf%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/pc%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/blacklist%')  | web_logs.fulldom.like('%shieldcf.o2.co.uk/bbfc%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/testiwfblock1%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/iotdefaultsafety%') | web_logs.fulldom.like('%shieldav.o2.co.uk%') | web_logs.fulldom.like('%shieldcf.o2.co.uk%') | web_logs.fulldom.like('%ageverification.o2.co.uk%') | web_logs.fulldom.like('%parentalcontrol.o2.co.uk%'))).repartition(1000).select('dt', 'loc', 'fulldom', 'nsvsn', 'version', 'serverport', 'emsisdn', 'optimisedsize', 'sessionid', 'customrepgrp').withColumn("https", when(col("serverport") == "443", "HTTPS").otherwise("HTTP"))
    conditions = [eckoh.sessionid == radius.sessionid2]
    df3 = eckoh.join(radius, conditions, how='left').groupBy('dt', 'loc', 'fulldom', 'nsvsn', 'version', 'https', 'custavp1', 'customrepgrp')\
    .agg(sqlfunc.sum('optimisedsize').alias('bytesdl'), sqlfunc.count('*').alias('cnt'), sqlfunc.countDistinct('emsisdn').alias('users')).coalesce(1000)\
    .createOrReplaceTempView("temporarytable")
    finaldf = spark_session.sql("create table keenek1.eckoh_landing_pages_test as select loc, fulldom, nsvsn, version, https, custavp1, customrepgrp, bytesdl, cnt, users from temporarytable")
    print(datetime.now())
    print('Eckoh complete')
    print('unpersisting radius cache and extract usercat')
    radius.unpersist()
    print('end')
    print(datetime.now())
