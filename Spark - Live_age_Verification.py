#!/usr/bin/env python
from pyspark.sql import SparkSession
import pyspark.sql.functions as sqlfunc
import argparse, sys
from pyspark.sql import *
from pyspark.sql.functions import *
from datetime import datetime

#create a context that supports hive
def create_session(appname):
    spark_session = SparkSession\
        .builder\
        .appName(appname)\
        .master('yarn')\
        .config("hive.metastore.uris", "thrift://uds-far-mn1.dab.02.net:9083")\
        .enableHiveSupport()\
        .getOrCreate()
    return spark_session

### START MAIN ###
if __name__ == '__main__':
    spark_session = create_session('Eckoh')
    df1 = spark_session.table('udsapp.web').withColumn("fulldom", concat(col("domain"), col("url")))
    df_agg = df1.coalesce(1000)\
        .filter((df1.dt == 20200330) & (df1.fulldom.like('%assets.o2.co.uk/18plusaccess%') | df1.fulldom.like('%shieldcf.o2.co.uk/tesco/iwf%') | df1.fulldom.like('%shieldcf.o2.co.uk/tesco/pc%') | df1.fulldom.like('%shieldcf.o2.co.uk/tesco/blacklist%') | df1.fulldom.like('%giffgaff.com/mobile/over18%') | df1.fulldom.like('%shieldcf.o2.co.uk/giffgaff/blacklist%') | df1.fulldom.like('%shieldcf.o2.co.uk/giffgaff/iwf%') | df1.fulldom.like('%shieldcf.o2.co.uk/iwf%') | df1.fulldom.like('%shieldcf.o2.co.uk/pc%') | df1.fulldom.like('%shieldcf.o2.co.uk/blacklist%')  | df1.fulldom.like('%shieldcf.o2.co.uk/bbfc%') | df1.fulldom.like('%shieldcf.o2.co.uk/testiwfblock1%') | df1.fulldom.like('%shieldcf.o2.co.uk/iotdefaultsafety%') | df1.fulldom.like('%shieldav.o2.co.uk%') | df1.fulldom.like('%shieldcf.o2.co.uk%') | df1.fulldom.like('%ageverification.o2.co.uk%') | df1.fulldom.like('%parentalcontrol.o2.co.uk%')
))\
        .select('dt', 'loc', 'fulldom', 'nsvsn', 'version', 'serverport', 'emsisdn', 'optimisedsize', 'sessionid', 'customrepgrp')\
        .withColumn("https", when(col("serverport") == "443", "HTTPS").otherwise("HTTP"))

    df2 = spark_session.table('udsapp.radius').withColumnRenamed('emsisdn', 'emsisdn2').withColumnRenamed('sessionid', 'sessionid2')
    df2_agg = df2.coalesce(1000).filter((df2.dt == 20200330)).select('emsisdn2', 'sessionid2', 'custavp1')

    conditions = [df_agg.sessionid == df2_agg.sessionid2, df_agg.emsisdn == df2_agg.emsisdn2]
    df3 = df_agg.join(df2_agg, conditions, how='left')

    #def shield(custrepgrp, custvap1):
        #if custavp1 == '':
           #shield == customrepgrp
        #else:
           #shield == custavp1
        #return shield

    #shield_udf = udf(shield)

    #df4 = df3.withColumn("shield", shield_udf(df3.custavp1, df3.custavp1))

    consolidated = df3.groupBy('dt', 'loc', 'fulldom', 'nsvsn', 'version', 'https', 'custavp1', 'customrepgrp').agg(sqlfunc.sum('optimisedsize').alias('bytesdl'), sqlfunc.count('*').alias('cnt'), sqlfunc.countDistinct('emsisdn').alias('users'))

    consolidated.createOrReplaceTempView("temporarytable")
    finaldf = spark_session.sql("create Table keenek1.dftest7 select * from temporarytable")
~