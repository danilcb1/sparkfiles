#!/usr/bin/env python
#nohup spark-submit --conf spark.driver.maxResultSize=0 --conf spark.network.timeout=800 --driver-memory 40g --executor-memory 70g 20200504_v1.py >stdout_xx2.log 2>stderr.log &
from pyspark.sql import SparkSession
import pyspark.sql.functions as sqlfunc
import argparse, sys
from pyspark.sql import *
from pyspark.sql.functions import *
from datetime import datetime
from pyspark.sql.functions import lit
from pyspark.sql.types import *
from pyspark.sql.functions import udf

now = datetime.now()
yday  = long(now.strftime('%s')) - 1*24*60*60
yday_date = datetime.fromtimestamp(yday).strftime('%Y%m%d')

radiusday  = long(now.strftime('%s')) - 3*24*60*60
radius_date = datetime.fromtimestamp(radiusday).strftime('%Y%m%d')


'''
drop table keenek1.ipfr_filtering2_test;
drop table keenek1.web_cf4_test;
drop table keenek1.eckoh_landing_pages_test;
drop table keenek1.testingcoverage_bd;
drop table keenek1.coverage_test;
drop table keenek1.testing_iphonesw_bd;
'''
#create a contexit that supports hive
def create_session(appname):
    spark_session = SparkSession\
        .builder\
        .appName(appname)\
        .master('yarn')\
        .enableHiveSupport()\
        .getOrCreate()
    return spark_session

### START MAIN ###
if __name__ == '__main__':
    print('start')
    print(datetime.now())
    '''
    TABLE DEFINITION AND CACHING
    '''
    spark_session = create_session('Combined_Web_IPFR_Radius')
    ipfr_logs = spark_session.sql("Select dt, hour, loc, lkey, domain, catid, cell_id, catreputation, serverport, catgrpid, urlsetmatched, responderaction, urlsetprivate, sid, emsisdn, bytesdl, bytesul, transactiontime, vservername, marketing_name, csp, easting, northing, csr, generation, sector, postcode  from udsapp.ipfr where dt = " + yday_date ).coalesce(1000).persist()
    x = ipfr_logs.count()
    print(x)
    web_logs = spark_session.sql("Select dt, vslsessid, customrepgrp, hour, nsvsn, urlfcid, serverport, urlfcgid, urlfcrep, matchedurlset, urlfstprv, urlfrspa, loc, emsisdn, sessionid, optimisedsize, concat(domain,url) as fulldom, domain, nsvsn, version from udsapp.web where dt = " + yday_date ).coalesce(1000).persist()
    y = web_logs.count()
    print(y)
    radius = spark_session.sql("Select custavp1, emsisdn as emsisdn2, sessionid as sessionid2 from udsapp.radius where cmdcode = 'PCRF_CCAI' and dt > " + radius_date ).coalesce(1000).persist()
    z = radius.count()
    print(z)
    fanbase = spark_session.sql("Select msisdn, avg_3_mths_spend from keenek1.latest_fanbase").coalesce(1000).persist()
    a = fanbase.count()
    print(a)
    usercat = spark_session.sql("Select tacmodelname, svset, imsi from incas.userscatalog_v1 where dt = " + yday_date).coalesce(1000).persist()
    b = usercat.count()
    print(b)
    print('caching IPFR, Web, Fanbase, Radius, UserCat complete')
    print(datetime.now())

    #IPFR CF
    ipfr = ipfr_logs.select('loc', 'catreputation', 'dt', 'hour', 'catid', 'catreputation', 'catgrpid', 'serverport', 'urlsetmatched', 'responderaction', 'urlsetprivate', 'sid', 'emsisdn', 'bytesdl', 'bytesul', 'vservername')
    conditions = [ipfr.sid == radius.sessionid2]
    df3 = ipfr.join(radius, conditions, how='left')\
    .groupBy('hour', 'catid', 'catreputation', 'catgrpid', 'vservername', 'loc', 'serverport', 'urlsetmatched', 'responderaction', 'urlsetprivate', 'custavp1')\
    .agg(sqlfunc.sum('bytesdl').alias('bytesdl'), sqlfunc.countDistinct('emsisdn').alias('users'), sqlfunc.count('*').alias('transactions'))\
    .createOrReplaceTempView('ipfr')
    ipfr_table_output = spark_session.sql('create table keenek1.ipfr_filtering2_test as  select "ipfr" as type, * from ipfr ')
    print(datetime.now())
    print('ipfr CF complete')

    #WEB CF
    web = web_logs.filter((web_logs.vslsessid == '0')).select('dt', 'customrepgrp', 'hour', 'nsvsn', 'urlfcid', 'serverport', 'urlfcgid', 'urlfcrep', 'matchedurlset', 'urlfstprv', 'urlfrspa', 'loc', 'emsisdn', 'sessionid', 'optimisedsize')
    conditionsx = [web.sessionid == radius.sessionid2]
    web_joined = web.join(radius, conditionsx, how='left').withColumn('actual_shield', when(radius.custavp1.isNull(), web.customrepgrp).otherwise(radius.custavp1))\
    .groupBy('hour', 'urlfcid', 'urlfcrep', 'urlfcgid', 'loc',  'serverport', 'matchedurlset', 'urlfstprv', 'nsvsn', 'actual_shield')\
    .agg(sqlfunc.sum('optimisedsize').alias('bytesdl'), sqlfunc.countDistinct('emsisdn').alias('users'), sqlfunc.count('*').alias('transactions'))\
    .createOrReplaceTempView('web')
    web_table_output = spark_session.sql('create table keenek1.web_cf4_test as select "web" as type, * from web')
    print(datetime.now())
    print('web CF complete')

    #ECKOH PAGES AGE VERIFICATION
    eckoh = web_logs.filter((web_logs.fulldom.like('%assets.o2.co.uk/18plusaccess%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/tesco/iwf%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/tesco/pc%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/tesco/blacklist%') | web_logs.fulldom.like('%giffgaff.com/mobile/over18%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/giffgaff/blacklist%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/giffgaff/iwf%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/iwf%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/pc%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/blacklist%')  | web_logs.fulldom.like('%shieldcf.o2.co.uk/bbfc%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/testiwfblock1%') | web_logs.fulldom.like('%shieldcf.o2.co.uk/iotdefaultsafety%') | web_logs.fulldom.like('%shieldav.o2.co.uk%') | web_logs.fulldom.like('%shieldcf.o2.co.uk%') | web_logs.fulldom.like('%ageverification.o2.co.uk%') | web_logs.fulldom.like('%parentalcontrol.o2.co.uk%'))).repartition(1000).select('dt', 'loc', 'fulldom', 'nsvsn', 'version', 'serverport', 'emsisdn', 'optimisedsize', 'sessionid', 'customrepgrp').withColumn("https", when(col("serverport") == "443", "HTTPS").otherwise("HTTP"))
    conditions = [eckoh.sessionid == radius.sessionid2]
    df3 = eckoh.join(radius, conditions, how='left').groupBy('dt', 'loc', 'fulldom', 'nsvsn', 'version', 'https', 'custavp1', 'customrepgrp')\
    .agg(sqlfunc.sum('optimisedsize').alias('bytesdl'), sqlfunc.count('*').alias('cnt'), sqlfunc.countDistinct('emsisdn').alias('users'))\
    .createOrReplaceTempView("temporarytable")
    finaldf = spark_session.sql("create table keenek1.eckoh_landing_pages_test as select loc, fulldom, nsvsn, version, https, custavp1, customrepgrp, bytesdl, cnt, users from temporarytable")
    print(datetime.now())
    print('Eckoh complete')
    print('eckoh pages complete')
    print(datetime.now())

    #coverage checks
    df_agg = ipfr_logs\
    .filter((ipfr_logs.domain.like("%%cdn.spatial%%"))).select("hour", "domain", "marketing_name", "emsisdn", "cell_id", "lkey", "csp", "easting", "northing", "postcode", "csr", "generation", "sector" )\
        .withColumn("long", sqlfunc.lit(0)).withColumn("lat", sqlfunc.lit(0))\
        .withColumn("new_postcode",\
        when(sqlfunc.length(sqlfunc.translate(sqlfunc.col("postcode"), " ", ""))  == 5, sqlfunc.col("postcode").substr(0,2))\
        .when(sqlfunc.length(sqlfunc.translate(sqlfunc.col("postcode"), " ", "")) == 6, sqlfunc.col("postcode").substr(0,4))\
        .when(sqlfunc.length(sqlfunc.translate(sqlfunc.col("postcode"), " ", "")) == 7, sqlfunc.col("postcode").substr(0,3)))

    conditions = [df_agg.emsisdn == fanbase.msisdn]
    df3 = df_agg.join(fanbase, conditions, how="left")
    df3.createOrReplaceTempView('tt')
    finaldf = spark_session.sql("create table keenek1.testingcoverage_bd as select hour, domain, marketing_name, emsisdn, cell_id, lkey, csp, easting, northing, new_postcode as postcode, csr, lat, long, generation, sector, avg_3_mths_spend from tt")
    print('Coverage check complete')
    print(datetime.now())

    #iphone version
    df1_agg = usercat.filter((sqlfunc.lower(sqlfunc.col("tacmodelname")).like("%iphone%"))).select(sqlfunc.lower(sqlfunc.col("tacmodelname")).alias("tacmodelname"), "svset", "imsi")
    df2 = spark_session.table("tableau_analytics.iphone_mapping")
    df2_agg = df2.coalesce(1000).select(sqlfunc.lower(sqlfunc.col("svn_name")).alias("svn_name"),"usercatname")
    conditions = [df1_agg.tacmodelname == df2_agg.usercatname]
    df3 = df1_agg.join(df2_agg, conditions, how="left")
    df4 = spark_session.table("udsapp.svn_mapping")
    df4_agg = df4.coalesce(1000).select("software_version", "svn",sqlfunc.lower(sqlfunc.col("device")).alias("device"))
    conditions1 = [(sqlfunc.regexp_extract(df3.svset,"[0-9]*$",0) == df4_agg.svn) & (df3.svn_name == df4_agg.device)]
    df5 = df3.join(df4_agg, conditions1, how="left")
    consolidated = df5.groupBy("tacmodelname", "svset", "svn_name", "software_version", "svn")\
    .agg(sqlfunc.countDistinct("imsi").alias("ttlimsi"))
    consolidated.createOrReplaceTempView("tt2")
    finaldf = spark_session.sql("create table keenek1.testing_iphonesw_bd as select * from tt2")
    print('iphone version complete')
    print(datetime.now())

    '''
    #domain normalisation
    toplevel = ['.co.uk', '.co.nz', '.com', '.net', '.uk', '.org', '.ie', '.it', '.gov.uk', '.news', '.co.in',
      '.io', '.tw', '.es', '.pe', '.ca', '.de', '.to', '.us', '.br', '.im', '.ws', '.gr', '.cc', '.cn', '.me', '.be',
      '.tv', '.ru', '.cz', '.st', '.eu', '.fi', '.jp', '.ai', '.at', '.ch', '.ly', '.fr', '.nl', '.se', '.cat', '.com.au',
      '.com.ar', '.com.mt', '.com.co', '.org.uk', '.com.mx', '.tech', '.life', '.mobi', '.info', '.ninja', '.today', '.earth', '.click']
    def cleanup(domain):
        for tld in toplevel:
                if tld in domain:
                        splitdomain = domain.split('.')
                        ext = tld.count('.')
                        if ext == 1:
                            cdomain = domain.split('.')[-2] + '.' + domain.split('.')[-1]
                            return cdomain
                        elif ext == 2:
                            cdomain = domain.split('.')[-3] + '.' + domain.split('.')[-2] + '.' + domain.split('.')[-1]
                            return cdomain
                        else:
                            return domain

    udfdict = udf(cleanup,StringType())
    output = web_logs.withColumn('capital',udfdict(web_logs.domain)).createOrReplaceTempView('webdoms')
    web_table_output = spark_session.sql('create table keenek1.cleanupdoms1 as select dt, hour, nsvsn, loc, capital, count(distinct(emsisdn)) as users, sum(optimisedsize) as size from webdoms group by dt, hour, nsvsn, loc, capital')
    print('web TLD domains complete')
    print(datetime.now())
    '''