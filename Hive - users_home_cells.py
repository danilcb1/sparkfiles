#!/usr/bin/env python

from pyspark.sql import SparkSession
from datetime import datetime
spark = SparkSession\
.builder\
.appName('wap')\
.master('yarn')\
.enableHiveSupport()\
.getOrCreate()

now = datetime.now()
yday  = long(now.strftime('%s')) - 24*60*60
yday_date = datetime.fromtimestamp(yday).strftime('%Y%m%d')

now_minus_3hrs  = long(now.strftime('%s')) - 5*60*60
now_date = datetime.fromtimestamp(now_minus_3hrs).strftime('%Y%m%d')
now_hour = datetime.fromtimestamp(now_minus_3hrs).strftime('%H')
print("WAP")
#The below prints your results to your chosen destination (Hive, Stdout, CSV)
cmd = '''
insert overwrite table keenek1.users_home
select
     emsisdn,
     postcode,
     lkey,
     site_name
from
(
select
    emsisdn,
    postcode,
    lkey,
site_name,
    ROW_NUMBER() over(partition by emsisdn order by transactions desc) rn
 from (select
         emsisdn, ipfr.postcode, ipfr.lkey, site_name, count(*) as transactions
         from udsapp.ipfr
left join udsapp.magnet on ipfr.lkey = magnet.lkey
where ipfr.dt = cast(regexp_replace(date_sub(current_date, 1),'-','') as int) and
           magnet.dt = cast(regexp_replace(date_sub(current_date, 1),'-','') as int)
      group by emsisdn, ipfr.postcode, ipfr.lkey, site_name
      )p
   group by emsisdn, postcode, site_name, lkey, transactions
    )s
where rn = 1
'''
spark.sql(cmd)