Spark
---
Covid Attacked Cells - Spark script : multiple joins; filters; includes IN LIST
Live age verification - Spark DF, new colums, case statements, joins
Concurrent Sessions Pandas - Ingest in Spark; convert to PD DF and Copy to HDFS
fast consolidated - joins; complex where; caching; aggregations
Domain cleanup: remove subdomains using UDF
DPI report - standard basic aggregations
use filepath - when reading structType; pull filepath for loc data
Current consolidation - string functions; joins; caching; coalesce; aggregation


Hive 
---
Users home cells - Hive script, using Rank & partition over functionality 
Radius concurrent session - Hive script using Lead & partition functionality
No Policy Match - Hive script, collect set & array functions
Rank transactions in 4 hour blocks - case statements; ranking; partitioning; lead and lag

Python
---
Main Table Monitor - Python script, using Email and OS functionality
Python HDFS Monitoring - Python script using email and OS functionality 
