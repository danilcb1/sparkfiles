#!/bin/bash
export SPARK_HOME=/usr/hdp/current/spark2-client
export PYTHONPATH=$SPARK_HOME/python:$SPARK_HOME/python/lib/py4j-0.10.6-src.zip:$SPARK_HOME/python/lib/pyspark.zip
export PATH=/app/anaconda2/bin:/usr/lib64/qt-3.3/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/dell/srvadmin/bin:/home/keenek1/bin:$SPARK_HOME/bin:$PATH
export  SPARK_MAJOR_VERSION=2
spark-submit --master yarn --deploy-mode cluster /home/keenek1/analytics/py_files/udftesting.py >stdout.log 2>stderr.log