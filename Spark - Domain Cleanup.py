#spark-submit --master yarn --deploy-mode cluster /home/keenek1/analytics/py_files/udftesting.py >stdout.loh 2>stderr.log
#!/usr/bin/env python
from pyspark.sql import SparkSession
import pyspark.sql.functions as sqlfunc
import argparse, sys
from pyspark.sql import *
from pyspark.sql.functions import *
from datetime import datetime
from pyspark.sql.functions import lit
from pyspark.sql.types import *
from pyspark.sql.functions import udf

#create a context that supports hive
def create_session(appname):
    spark_session = SparkSession\
        .builder\
        .appName(appname)\
        .master('yarn')\
        .enableHiveSupport()\
        .getOrCreate()
    return spark_session

### START MAIN ###
if __name__ == '__main__':
    '''
    TABLE DEFINITION AND CACHING
    '''
    toplevel = ['.co.uk', '.co.nz', '.com', '.net', '.uk', '.org', '.ie', '.it', '.gov.uk', '.news', '.co.in',
      '.io', '.tw', '.es', '.pe', '.ca', '.de', '.to', '.us', '.br', '.im', '.ws', '.gr', '.cc', '.cn', '.me', '.be',
      '.tv', '.ru', '.cz', '.st', '.eu', '.fi', '.jp', '.ai', '.at', '.ch', '.ly', '.fr', '.nl', '.se', '.cat', '.com.au',
      '.com.ar', '.com.mt', '.com.co', '.org.uk', '.com.mx', '.tech', '.life', '.mobi', '.info', '.ninja', '.today', '.earth', '.click']

    def cleanup(domain):
        for tld in toplevel:
                if tld in domain:
                        splitdomain = domain.split('.')
                        ext = tld.count('.')
                        if ext == 1:
                            cdomain = domain.split('.')[-2] + '.' + domain.split('.')[-1]
                            return cdomain
                        elif ext == 2:
                            cdomain = domain.split('.')[-3] + '.' + domain.split('.')[-2] + '.' + domain.split('.')[-1]
                            return cdomain
                        else:
                            return domain

    spark_session = create_session('Clean_domains')
    web_logs = spark_session.sql("Select domain from udsapp.web where dt = 20200420 and hour = 04" )

    '''
    WEB CF
    '''
    udfdict = udf(cleanup,StringType())
    output = web_logs.withColumn('CAPITAL',udfdict(web_logs.domain)).createOrReplaceTempView('web')
    web_table_output = spark_session.sql('insert overwrite table keenek1.cleanupdoms1 select * from web')