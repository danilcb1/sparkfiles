#!/usr/bin/env python
from pyspark.sql import SparkSession
import pyspark.sql.functions as sqlfunc
import argparse, sys
from pyspark.sql import *
from pyspark.sql.functions import *
from datetime import datetime
from pyspark.sql.functions import lit
from pyspark.sql.types import *

now = datetime.now()
yday  = long(now.strftime('%s')) - 1*24*60*60
yday_date = datetime.fromtimestamp(yday).strftime('%Y%m%d')


#create a context that supports hive
def create_session(appname):
    spark_session = SparkSession\
        .builder\
        .appName(appname)\
        .master('yarn')\
        .config("hive.metastore.uris", "thrift://uds-far-mn1.dab.02.net:9083")\
        .enableHiveSupport()\
        .getOrCreate()
    return spark_session
if __name__ == '__main__':
    '''
    TABLE DEFINITION 
    '''
    spark_session = create_session('Clean DPI Report')
    dpi_data_logs = spark_session.sql("Select dt, hour, protocol_category, service_identifier, sum(service_dl_bytes) as dl_bytes, sum(service_ul_bytes) as ulbytes, csp, mcc, imsi count(distinct(imsi)) as users, count(*) as transactions, loc from udsapp.dpi_data where dt = " + yday_date + " group by dt, hour, protocol_category, service_identifier, csp, mcc, loc" )
    countries = spark_session.sql("Select mcc as mcc2, country, eu, o2travel, c27, zone from keenek1.mcctocountry_new")

    '''
    DPI
    '''
    dpi_data = dpi_data_logs.join(countries, dpi_data_logs.mcc == countries.mcc2, how='left').createOrReplaceTempView('dpi')
    dpi_table_output = spark_session.sql(' create table design.dpi as select  * from dpi ')
