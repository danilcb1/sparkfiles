from subprocess import check_output
import subprocess
import csv
from io import StringIO
import smtplib
from datetime import datetime
import matplotlib
matplotlib.use('Agg')
import pandas as pd

command = 'hdfs dfs -du /data/ebm/data'
out = check_output(command, shell=True)
df = pd.DataFrame([x.split('\t') for x in out.split('\n')])
df.columns = ['input']
df.drop(df.tail(6).index,inplace=True)

def splitit(row):
    size = int(str(row['input']).split(' ')[0])
    return size

print(df)
def get_date(row):
    date = str(row['input'])[-8:]
    try:
        date = date[-2:] + '-' + date[4:6] + '-' + date[:4]
    except:
        date = 'invalid'
    return date

df['size'] = df.apply(splitit, axis=1)
df['date'] = df.apply(get_date, axis=1)

#df.to_csv('/home/keenek1/analytics/py_files/dpi_mon.csv')

def get_file_count(row):
    try:
        edir = str(row['input']).split('  ')[1]
    except:
        edir = 'none'
    print(edir)
    cmd = 'hdfs dfs -count ' + edir
    try:
        cmdout = check_output(cmd, shell=True)
    except:
        cmdout = '0    0    0    0    0    0'
    cmdout = "|".join(cmdout.split())
    print(cmdout)
    count = int(cmdout.split('|')[1])
    print(count)
    return count

df['files'] = df.apply(get_file_count, axis=1)

df = df[['date', 'size', 'files']]

pd.set_option('display.float_format', lambda x: '%.3f' % x)

df['prev_size'] = df['size'].shift(1)

df['prev_count'] = df['files'].shift(1)

df['pct_change_volume_from_yesterday'] = ((df['size'] / df['prev_size'])*100)
df['pct_change_files_from_yesterday'] = ((df['files'] / df['prev_count'])*100)
df['pct_change_from_mean_volume'] = ((df['size']/df["size"].mean())*100)
df['pct_change_from_mean_files'] = ((df['files']/df["files"].mean())*100)

def status (row):
    if row['pct_change_volume_from_yesterday'] > 40 or row['pct_change_volume_from_yesterday'] < 40:
        status = 'Not OK'
    else:
        status = 'OK'
    return status


def files_status (row):
    if row['pct_change_files_from_yesterday'] > 40 or row['pct_change_files_from_yesterday'] < 40:
        status = 'Not OK'
    else:
        status = 'OK'
    return status

def isweekday(row):
    date = datetime.strptime(row['date'], '%d-%m-%Y')
    day = date.weekday()
    if day <= 4:
        weekday = 'Weekday'
    else:
        weekday = 'Weekend'
    return weekday

df['vol_ok'] = df.apply(status, axis=1)
df['file_ok'] = df.apply(files_status, axis=1)
df['type'] = df.apply(isweekday, axis=1)

#drop obvious NULL increases
df = df.iloc[3:]

dffiles = df[['date', 'files']]
plot = dffiles.plot(kind='bar', title ="File Counts")
fig = plot.get_figure()
fig.savefig("output.png")

email = " <h3>DPI Data Records:</h3> {df}"
email = email.format(df=df.to_html())


message = """From: From Netpulse Source Table Monitoring <kieran.keene@o2.com>
To: To Kieran Keene <kieran.keene@o2.com>
MIME-Version: 1.0
Content-type: text/html
Subject: Daily Netpulse Source Table  Monitoring Results

This is an e-mail message reporting on the status of the main Netpulse feeds <br>
**********************************"""  +  email + '<img src="output.png">'
#message = "Logs output" + data




print(message)

sender = 'kieran.keene@o2.com'
receivers = ['kieran.keene@o2.com']

try:
    smtpObj = smtplib.SMTP('mailhost.uk.pri.o2.com')
    smtpObj.sendmail(sender, receivers, message)
    print("Successfully sent email")
except SMTPException:
    print("Error: unable to send email")


import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
#from email.MIMEImage import MIMEImage
from email.mime.image import MIMEImage

msg = MIMEMultipart('related')
msg['Subject'] = "My text dated "
msg['From'] = sender
receiver = 'kieran.keene@o2.com'
msg[receiver] = receiver

html = """From: From Netpulse Source Table Monitoring <kieran.keene@o2.com>
To: To Kieran Keene <kieran.keene@o2.com>
MIME-Version: 1.0
Content-type: text/html
Subject: Daily Netpulse Source Table  Monitoring Results

This is an e-mail message reporting on the status of the main Netpulse feeds <br>
**********************************"""  +  email


# Record the MIME types of text/html.
part2 = MIMEText(html, 'html')

# Attach parts into message container.
msg.attach(part2)

# This example assumes the image is in the current directory
fp = open('output.png', 'rb')
msgImage = MIMEImage(fp.read())
fp.close()

# Define the image's ID as referenced above
msgImage.add_header('Content-ID', '<image1>')
msg.attach(msgImage)

# Send the message via local SMTP server.
mailsrv = smtplib.SMTP('mailhost.uk.pri.o2.com')
mailsrv.sendmail(sender, receiver, msg.as_string())
mailsrv.quit()